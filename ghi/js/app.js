function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card mb-5 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${new Date(starts).toLocaleDateString()} -
            ${new Date(ends).toLocaleDateString()}
      </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/'
    try {
    const response = await fetch(url)
     if(!response.ok) {
        console.log("Invalid response")
     }else{
        const data = await response.json()
        const column = document.querySelectorAll('.col')
        let index = 0
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`
            const detailResponse = await fetch(detailUrl)
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const location = details.conference.location.name;
                const starts = details.conference.starts;
                const ends = details.conference.ends;
                const pictureUrl = details.conference.location.picture_url;
                const html = createCard(title, description, pictureUrl, starts, ends, location);
                column[index].innerHTML += html;
                if (index === 2) {
                  index = 0
                }else{
                  index++
                }
            }
        }

     }
    }catch (e) {
        console.error("Error has occured", e)
    }
})
